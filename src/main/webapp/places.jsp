<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<meta charset="utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/jquery-3.0.0.js"></script>
</head>

<body>
	<div class="wrapper">


		<div class="b-page__line">
			<div class="b-menu">
				<a href="places" class='b-link b-link_menu b-link_menu_active'>HOME</a>
				<a href="#" class='b-link b-link_menu'>ABOUT US</a> <a
					href="agreement" class='b-link b-link_menu'>SERVICES</a> <a
					href="#" class='b-link b-link_menu'>CUSTOMERS</a> <a href="#"
					class='b-link b-link_menu'>CONTACT</a>
			</div>
		</div>

		<div class="middle">

			<div class="container" id="1234">
				<main class="content" id="mainClass"> 
				<c:forEach items="${textA}" var="element">
					<div id="${element.placeID}" class="parking-place">
						<c:choose>
							<c:when test="${element.avaliable==true}">
								<img src="${pageContext.request.contextPath}/source/green.png">
							</c:when>
							<c:when test="${element.avaliable==false}">
								<img src="${pageContext.request.contextPath}/source/red.png">
							</c:when>
							<c:otherwise>
								<img />
							</c:otherwise>
						</c:choose>
						<sec:authorize access="hasRole('CHIEF_MANAGER')">
							<button class="delete-parking-place">DELETE</button>
						</sec:authorize>
						
						
					</div>
					
				</c:forEach> </main>
				<!-- .content -->

			</div>
			<!-- .container-->

		</div>
		<!-- .middle-->

	</div>
	<!-- .wrapper -->

	<div id="userInfo">
	<sec:authorize access="isAuthenticated()">
   		 <sec:authentication property="principal.firstname" var="username" />
   		 <sec:authentication property="principal.email" var="email" />
   		 <sec:authentication property="principal.birthdate" var="birthdate" />
	</sec:authorize>
		Hello, <br> 
		${username}<br> 
		${email}<br>
		${birthdate}<br>
		<form:form action="logout" method="get">
			<input type="submit" value="Logout">
		</form:form>
	</div>


	<div id="popup">

		<div class="info" id="sendform">
			<span class="popup-close" id='close'>x</span>
			<form:form method="post" id='regform'>
				First name:<br> 
				<input type="text" name="firstname" pattern="[a-zA-Z]{4,}" required><br> 
				<input type="checkbox" name="checkbox" required>
				I have read the <a href="${pageContext.request.contextPath}/agreement.jsp" target="_blank">user agreement</a>
				<button type="submit" id="regform_submit">Submit</button>
			</form:form>
		</div>
		<div class="info" id="error">
			Parking place is reserved
			<button class="popup-close">OK</button>
		</div>
	</div>

	<script src="${pageContext.request.contextPath}/js/popup.js" async></script>
	<script src="${pageContext.request.contextPath}/js/pplaces.js"></script>
</body>
</html>