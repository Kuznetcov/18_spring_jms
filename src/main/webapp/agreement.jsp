<html>
<head>
<meta charset="utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/popup.js" async></script>

</head>

<body>
	<div class="wrapper">


		<div class="b-page__line">
			<div class="b-menu">
				<a href="places" class='b-link b-link_menu'>HOME</a>
				<a href="#" class='b-link b-link_menu'>ABOUT US</a> 
				<a href="agreement" class='b-link b-link_menu b-link_menu_active'>SERVICES</a> 
				<a href="#" class='b-link b-link_menu'>CUSTOMERS</a> 
				<a href="#" class='b-link b-link_menu'>CONTACT</a>
			</div>
		</div>

		<div class="middle">

			<div class="container" id="1234">
				<main class="content" id="mainClass">
				
				<div class ="content-text">
				User agreement
				
				TODO
				</div>
				
				</main>
				<!-- .content -->

			</div>
			<!-- .container-->

		</div>
		<!-- .middle-->

	</div>
	<!-- .wrapper -->


	<div id="popup">
		
		<div class="info" id="sendform">
			<span id='close' onclick='hidePopup()'>x</span>
			<form method="post">
				First name:<br>
				<input type="text" name="firstname"><br>
				<input type="checkbox" name="checkbox">I have read the <a href="">user agreement</a>
				<button onclick="sendUserData()">Submit</button>
			</form>
		</div>
		<div class="info" id="error">
			Parking place is reserved
			<button onclick="hidePopup()">OK</button>
		</div>
	</div>
</body>
</html>