var id;
$(document).on('click','.parking-place',function(event){
			if (event.target.className != "delete-parking-place") {
				showPopup();
			} else {
				id = event.target.parentNode.id;
				window.location.href = "/mvcbased/inside/deletePlace?id="+id
			};
			var car;
			if (event.target.tagName == "IMG") {
				car = event.target;
				id = event.target.id;
			}
			if (event.target.className == "parking-place") {
				car = event.target.children[0];
				id = event.target.id;
			}
			if (car.src.indexOf("red.png") != -1){
				$('#error').show();
			} else {
				$('#sendform').show();
			}
		}
	);
$(document).on('click','.popup-close',function(event){
		hidePopup();
		}
	);
$(document).on('submit','#regform',function(event){
	event.preventDefault();
	sendUserData(this);
	}
);
function showPopup() {
	$('#popup').show();
}

function hidePopup() {
	$('#popup').hide();
	$('#error').hide();
	$('#sendform').hide();
}

function sendUserData(f){
	if (validateForm(f)){
		var field1 = f.elements["firstname"].value;
		var field2 = f.elements["checkbox"].value;
		console.log("Параметры: " +field1 + " " +field2 + " отправлены");
	}
	
	var form = $(f);
	
	var index = id;
	
    $.ajax({
      type: 'post',
      url: "/mvcbased/inside/blockparkingplace",
      data: form.serialize(),
      beforeSend:function(xhr, settings){
    	     settings.data += '&id='+index;
    	     }
    }).done(function(data) {
    	$('#mainClass').empty();
    	$.get(
    			  "/mvcbased/restws/placeslist",
    			  onAjaxSuccess
    			);
    	
    }).fail(function(data) {
      // Optionally alert the user of an error here...
    });
	hidePopup();
}

function onAjaxSuccess(data)
{
  $.each(data, function() {
	  
	  if (this.avaliable==true) {
		  var html = '<div id="' + this.placeID + '" class="parking-place"><img src="/mvcbased/source/green.png"></div>';
	  } else {
		  var html = '<div id="' + this.placeID + '" class="parking-place"><img src="/mvcbased/source/red.png"></div>';
	  }
      $('#mainClass').append(html);
   });
  console.log("Page refreshed. Parking places updated");
}

function validateForm(f){
	if(!f.elements["checkbox"].checked) {
			alert("Checkbox is empty!"); 
			return false;
		} 
		else {
			return true;
		};
}