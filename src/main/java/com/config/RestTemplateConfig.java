package com.config;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.dal.ModelComposer;
import com.dal.ModelComposerJmsIml;


@Configuration
@EnableCaching
public class RestTemplateConfig {

  @Bean
  public ModelComposer modelComposer() {
    return new ModelComposerJmsIml();
  }
  
  @Bean
  public ClientHttpRequestFactory httpRequestFactory() {
    HttpClient client = HttpClientBuilder.create().build();
   return new HttpComponentsClientHttpRequestFactory(client);
  }
  
  @Bean
  public RestTemplate restTemplate() {
   RestTemplate restTemplate = new RestTemplate(httpRequestFactory());
  return restTemplate;
  }
  
  @Bean
  public CacheManager cacheManager() {
      return new ConcurrentMapCacheManager("places");
  }
  
}
