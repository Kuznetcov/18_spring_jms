package com.config;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;

@Configuration
@EnableJms
public class JmsConfig {

  @Bean
  public ActiveMQConnectionFactory amqConnectionFactory(){  
    return new ActiveMQConnectionFactory("tcp://localhost:61616");
  }
  
  @Bean
  @Autowired
  public ConnectionFactory connectionFactory(ActiveMQConnectionFactory amqConnectionFactory){
    return new CachingConnectionFactory(amqConnectionFactory);
  }
  
  @Bean
  public ActiveMQQueue defaultDestination(){
    return new ActiveMQQueue("Send2Recv");
  }
  
  @Bean
  @Autowired
  public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory){
    JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
    jmsTemplate.setDefaultDestination(defaultDestination());
    return jmsTemplate;
  }
  
  @Bean
  public MappingJackson2MessageConverter converter(){
    return new MappingJackson2MessageConverter();
  }
  
}
