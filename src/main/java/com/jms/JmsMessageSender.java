package com.jms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

@Service
public class JmsMessageSender {

  @Autowired
  private JmsTemplate jmsTemplate;
  
  public void send(final String text) {
    
    this.jmsTemplate.send(
        
     new MessageCreator() {
      @Override
      public Message createMessage(Session session) throws JMSException {
        Message message = session.createTextMessage(text);     
        //set ReplyTo header of Message, pretty much like the concept of email.
        message.setJMSReplyTo(new ActiveMQQueue("Recv2Send"));
        return message;
      }
      
    });
  }
  
  /**
   * Simplify the send by using convertAndSend
   * @param text
   */
  public void sendText(final String text) {
    this.jmsTemplate.convertAndSend(text);
  }
  
  /**
   * Send text message to a specified destination
   * @param text
   */
  public String send(final Destination dest,final String text) {
      
    Message message = this.jmsTemplate.sendAndReceive(dest,new MessageCreator() {
      @Override
      public Message createMessage(Session session) throws JMSException {
        Message message = session.createTextMessage(text);
        return message;
      }
    });
    return message.toString();
    
    
  }
  
  public Message send(final String destinationName,final String text) {
    
    Message message = this.jmsTemplate.sendAndReceive(destinationName,new MessageCreator() {
      @Override
      public Message createMessage(Session session) throws JMSException {
        Message message = session.createTextMessage(text);
        return message;
      }
    });
    return message;
    
    
  }
  
}
