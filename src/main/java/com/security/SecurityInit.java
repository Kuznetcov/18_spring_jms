package com.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SecurityInit extends AbstractSecurityWebApplicationInitializer {

  
//class AbstractSecurityWebApplicationInitializer will ensure the springSecurityFilterChain gets registered for you
//Automatically register the springSecurityFilterChain Filter for every URL in your application
  
//  AbstractSecurityWebApplicationInitializer provides the availability of 
//  DelegatingFilterProxy and ContextLoaderListener and are registered automatically. 
  
//В конструктор этого класса передается наш AppSecurityConfig. В коде мы этого сами не делаем, т.к. Spring делает это за нас. 
//но это надо было бы сделать, если бы SpringSecurity использовался без SpringCore/SpringMVC
}
