package com.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.controller.Login;
import com.dal.ModelComposer;
import com.data.Roles;
import com.data.User;

@Service
public class CustomUserDetailsService implements UserDetailsService {

  static Logger log = LoggerFactory.getLogger(Login.class);
  
  @Autowired
  ModelComposer mc;

  @Override
  public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
    CustomUserDetails loadedUser = null;
    User user = mc.getUserByLogin(login);
    log.info("By login:"+login+" recived user with data: "+user);
    List<GrantedAuthority> authList = getAuthorities(user.getRoles());
    loadedUser = new CustomUserDetails(user.getLogin(),
        user.getPassword(),true, true, true, true, authList);
    loadedUser.setEmail(user.getEmail());
    loadedUser.setBirthdate(user.getBirthdate());
    loadedUser.setFirstname(user.getUsername());
    return loadedUser;
  }

  private List<GrantedAuthority> getAuthorities(Set<Roles> rules) {
    List<GrantedAuthority> authList = new ArrayList<>();
    rules.stream().forEach((r) -> {
      authList.add(new SimpleGrantedAuthority("ROLE_"+r.getName()));
      log.info(r.getName()+ " role added");
    });
    return authList;
  }
}
