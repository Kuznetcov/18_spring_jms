package com.dal;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConversionException;

import com.data.ParkingPlace;
import com.data.PlaceEvent;
import com.data.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jms.JmsMessageSender;

public class ModelComposerJmsIml implements ModelComposer {

  @Autowired
  private JmsMessageSender jms;
  
  static Logger log = LoggerFactory.getLogger(ModelComposerJmsIml.class);
  
  @Override
  public List<ParkingPlace> getParkingPlaces() {
    
    List<ParkingPlace> list = null;

      try {
        TextMessage message = (TextMessage)jms.send("GetPlaces", "uselessstring");
        ObjectMapper objectMapper = new ObjectMapper();
        ParkingPlace[] places = objectMapper.readValue(message.getText(),ParkingPlace[].class);
        list = Arrays.asList(places);
      } catch (MessageConversionException | IOException | JMSException e) {
        log.error(e.getMessage());
      }

    return list;
  }

  @Override
  public User getUserByLogin(String login) {
    User user=null;
    try {
      TextMessage message = (TextMessage) jms.send("GetUser", login);
      ObjectMapper objectMapper = new ObjectMapper();
      user = objectMapper.readValue(message.getText(),User.class);
      log.info(message.toString());
    } catch (MessageConversionException | JMSException | IOException e) {
      log.error(e.getMessage());
    }
    return user;
  }
  
  @Override
  public String addEvent(PlaceEvent event) {
    ObjectMapper objectMapper = new ObjectMapper();
    String result = "Failed";
    try {
      TextMessage message = (TextMessage) jms.send("AddPlaceEvent", objectMapper.writeValueAsString(event));
      result = message.getText();
      log.info("Response code " + message.getText());
    } catch (JsonProcessingException | JMSException e) {
      log.error(e.getMessage());
    }
    return result;
  }


  @Override
  public String addUser(User user) {
    ObjectMapper objectMapper = new ObjectMapper();
    String result = "Failed";
    try {
      TextMessage message = (TextMessage) jms.send("AddUser", objectMapper.writeValueAsString(user));
      result = message.getText();
      log.info("Response code " + message.getText());
    } catch (JsonProcessingException | JMSException e) {
      log.error(e.getMessage());
    }
    return result;
  }

  @Override
  public String addParkingPlace(ParkingPlace place) {
    // TODO never!
    return null;
  }

  @Override
  public String deleteParingPlace(Integer id) {
    TextMessage message = (TextMessage) jms.send("DeleteParkingPlace", id.toString());
    try {
      log.info("Response code " + message.getText());
      return message.getText();
    } catch (JMSException e) {
      log.error(e.getMessage());
    }
    return null;
  }

  @Override
  public String addUser(String name, String login, String password) {
    // not used in JMS
    return null;
  }

  @Override
  public String getUser(String login, String password) {
    // not used in JMS
    return null;
  }
  
  @Override
  public List<ParkingPlace> getParkingPlacesByPage(Integer pageNumber) {
    //not used with JMS
    return null;
  }

  
  
}
