package com.controller;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.dal.ModelComposer;
import com.data.Event;
import com.data.PlaceEvent;

@Controller
@RequestMapping("/inside")

public class ParkingPlace {

  private Logger log = LoggerFactory.getLogger(ParkingPlace.class);
  
  @Autowired
  private ModelComposer modelComposer;

  
  @RequestMapping(value="/blockparkingplace",method = RequestMethod.POST)
  @ResponseStatus(value = HttpStatus.OK)
  public void checkLoginForm(ModelMap model, @RequestParam(value = "firstname") final String firstname,
      @RequestParam(value = "checkbox") final Boolean checkbox,
      @RequestParam(value = "id") final String id,
      HttpServletRequest request,
      HttpServletResponse response) {
    
    Event et = new Event("CAR ARRIVED");
    PlaceEvent event = new PlaceEvent();
    event.setEventID(new Random().nextInt(20000));
    event.date="2016-10-10";
    event.setPlaceID(Integer.valueOf(id));
    event.setEvent(et);
    modelComposer.addEvent(event);
    log.info("Params: "+event.getPlaceID() + " " +event.getDate() + " "+event.getEvent() + " "+event.getEventID() + " ");
  }

  @RequestMapping(value="/deletePlace",method = RequestMethod.GET)
  public String doGet(HttpServletRequest request, HttpServletResponse response,@RequestParam("id") String id)
      throws ServletException, IOException {
    modelComposer.deleteParingPlace(Integer.valueOf(id));
    return "redirect:places";
  }
  
}
