package com.controller.rest;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.dal.ModelComposer;
import com.data.ParkingPlace;

@RestController
@RequestMapping("/restws")
public class PlacesToJSON {

  @Autowired
  private ModelComposer modelComposer;

  @RequestMapping("/placeslist")
  public List<ParkingPlace> getPlacesList(@RequestParam(value = "available", required = false, defaultValue = "false") String available) {
    List<ParkingPlace> places = modelComposer.getParkingPlaces();
    if (!available.equals("true")) {
      return places;
    } else {
      List<ParkingPlace> placesWithAttribute = new ArrayList<>();
      places.stream().forEach((p) -> {if (p.getAvaliable()==true) placesWithAttribute.add(p);});
      return placesWithAttribute;
    }
  }
  
  @RequestMapping("/placeslist/{page}")
  public List<ParkingPlace> getPlacesListByPage(@PathVariable String page) {
    List<ParkingPlace> places = modelComposer.getParkingPlacesByPage(Integer.valueOf(page));
      return places;
  }
  

}
