package com.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.controller.data.SessionDataBean;
import com.dal.ModelComposer;
import com.data.User;

@Controller
@RequestMapping("/login")
public class Login {
  
  static Logger log = LoggerFactory.getLogger(Login.class);
  
  
  @Autowired
  private ModelComposer modelComposer;

  @Autowired
  private SessionDataBean sessionDataBean;

  @RequestMapping(method = RequestMethod.GET)
  public String showLogin(ModelMap model) {
    
    User userForm = new User();    
    model.put("userForm", userForm);
    return "login";
  }

  @RequestMapping(method = RequestMethod.POST)
  public String checkLoginForm(ModelMap model, @ModelAttribute("userForm") User user, HttpServletRequest request,
      HttpServletResponse response) {
    System.out.println(user.getLogin() + " " + user.getPassword());
    
    String username = modelComposer.getUser(user.getLogin(), user.getPassword());

    if (username != null) {
      Cookie cookie = new Cookie("friendlyusername", username);
      sessionDataBean.setLogin(user.getLogin());
      sessionDataBean.setName(username);
      sessionDataBean.setPassword(username);
      sessionDataBean.setSessionId(request.getSession().getId());
      response.addCookie(cookie);
      model.addAttribute("sessionBean", sessionDataBean);
      log.info("Login accepted: " + user.getLogin());
      return "redirect:places";
    } else {
      model.addAttribute("error", "User not found");
      return "error";
    }
  }
}
