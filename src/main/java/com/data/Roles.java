package com.data;

public class Roles {

  private Integer id;
  private String name;
  public Roles(Integer id, String name) {
    super();
    this.id = id;
    this.name = name;
  }
  public Roles() {
    super();
  }
  public synchronized Integer getId() {
    return id;
  }
  public synchronized void setId(Integer id) {
    this.id = id;
  }
  public synchronized String getName() {
    return name;
  }
  public synchronized void setName(String name) {
    this.name = name;
  }
  @Override
  public String toString() {
    return "Roles [id=" + id + ", name=" + name + "]";
  }
  
}
